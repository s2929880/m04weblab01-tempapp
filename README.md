# M04 - WebLab01 Report
### Nick Diamantopoulos - s2929880
[**GitLab repository**](https://gitlab.utwente.nl/s2929880/m04weblab01-tempapp "GitLab Repository")
## Completed Assignments
I was able to complete most assignments on my own until the Temperature Web Application implementation.

I had issues with the routing and after consulting a few TAs I re-did the assignment from the beginning and the problem seems to have stopped.

Everything was checked for any issues but nothing seemed to fix it. It is still ambiguous what caused the problem

## Accomplishments
After doing all exercises I was able to create a (relatively good-looking) web application to convert Celsius to Fahrenheit.

The application also includes fail-safes for when the user inputs wrong information into the system (non-numerical values in the input)

## Learning 
I was able to learn a lot about how servlets work and how java incorporates web development

I already have quite a bit of experience with html and css, so I wouldn't say I learned anything about them, but using requests and form fields this way was a first.

## Servlet Typical Execution Flow
The servlet typical execution flow is very apparent in this example as all steps are clearly showin in the code. 
* The server receives a requests 
* It passes it onto the servlet (`TemperatureConverter`)
* The `TemperatureConverter` processes the request (using the helper class `Converter`)
* Finally, the server sends its `HTML` response
