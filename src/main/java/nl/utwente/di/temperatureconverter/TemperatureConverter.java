package nl.utwente.di.temperatureconverter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class TemperatureConverter extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Converter converter;
	
    public void init() throws ServletException {
    	converter = new Converter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String c = request.getParameter("celsius");
    String f = converter.getFahrenheit(request.getParameter("celsius"));
    String htmlCode = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <title>Converted: "+c+"°C->"+f+"°F</title>\n" +
            "  <link rel=\"stylesheet\" href=\"styles.css\">\n" +
            "</head>\n" +
            "<body>\n" +
            "    <div class=\"surface\">\n" +
            "      <h1>Converted Temperature:</h1>\n" +
            "      <p>Celsius:"+c+"°C</p>\n" +
            "      <p>Fahrenheit:"+f+"°F</p>\n" +
            "    </div>\n" +
            "</body>\n" +
            "</html>";
    out.println(htmlCode);

  }
}
