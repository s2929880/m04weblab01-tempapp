package nl.utwente.di.temperatureconverter;

public class Converter {
    public String getFahrenheit(String temp) {
        try {
            double celcius = Double.parseDouble(temp);
            return String.valueOf((celcius * 9/5) + 32);
        } catch (Exception e) {
            return "-";
        }
    }
}
